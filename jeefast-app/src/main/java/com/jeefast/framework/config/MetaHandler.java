package com.jeefast.framework.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.jeefast.common.utils.SecurityUtils;
import com.jeefast.project.system.domain.SysUser;

import org.apache.ibatis.reflection.MetaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 处理新增和更新的基础数据填充，配合BaseEntity和MyBatisPlusConfig使用
 */
@Component
public class MetaHandler implements MetaObjectHandler {

    private static final Logger logger = LoggerFactory.getLogger(MetaHandler.class);

    /**
     * 新增数据执行
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        SysUser userEntity = SecurityUtils.getLoginUser().getUser();
        this.setFieldValByName("createTime", new Date(), metaObject);
        this.setFieldValByName("createBy", userEntity.getUserName(), metaObject);
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("updateBy", userEntity.getUserName(), metaObject);
    }

    /**
     * 更新数据执行
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
    	SysUser userEntity = SecurityUtils.getLoginUser().getUser();
        this.setFieldValByName("updateTime", new Date(), metaObject);
        this.setFieldValByName("updateBy", userEntity.getUserName(), metaObject);
    }

}
