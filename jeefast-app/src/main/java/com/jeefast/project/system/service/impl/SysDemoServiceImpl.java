package com.jeefast.project.system.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jeefast.project.system.domain.SysDemo;
import com.jeefast.project.system.mapper.SysDemoMapper;
import com.jeefast.project.system.service.ISysDemoService;

/**
 * 用户演示 服务层实现
 *
 * @author jeefast
 * @date 2019-09-24
 */
@Service
public class SysDemoServiceImpl extends ServiceImpl<SysDemoMapper, SysDemo> implements ISysDemoService {

}