package com.jeefast.project.system.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.jeefast.common.core.text.Convert;
import com.jeefast.common.utils.StringUtils;
import com.jeefast.framework.aspectj.lang.annotation.Log;
import com.jeefast.framework.aspectj.lang.enums.BusinessType;
import com.jeefast.framework.web.controller.BaseController;
import com.jeefast.framework.web.domain.AjaxResult;
import com.jeefast.framework.web.page.TableDataInfo;
import com.jeefast.project.system.domain.SysDemo;
import com.jeefast.project.system.service.ISysDemoService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;

/**
 * 用户演示Controller
 * 
 * @author jeefast
 * @date 2019-09-24
 */
@RestController
@RequestMapping("/system/demo")
@Api(tags = "Demo用户信息管理")
public class SysDemoController extends BaseController {
    private String prefix = "system/demo";

    @Autowired
    private ISysDemoService sysDemoService;

    @GetMapping()
    public String demo() {
        return prefix + "/demo";
    }

    /**
     * 查询用户演示列表
     */
    @PostMapping("/list")
    @ApiOperation("获取用户列表")
    public TableDataInfo list(SysDemo sysDemo) {
    	QueryWrapper<SysDemo> queryWrapper = new QueryWrapper<>();
    	// 需要根据页面查询条件进行组装
    	if(StringUtils.isNotEmpty(sysDemo.getLoginName())) {
    		queryWrapper.like("login_name", sysDemo.getLoginName());
    	} 
    	if(StringUtils.isNotEmpty(sysDemo.getUserName())) {
    		queryWrapper.like("user_name", sysDemo.getUserName());
    	}
        startPage();
        return getDataTable(sysDemoService.list(queryWrapper));
    }

    /**
     * 新增保存用户演示
     */
    @Log(title = "用户演示", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ApiOperation("新增用户")
    @ApiImplicitParam(name = "sysDemo", value = "新增用户信息", dataType = "SysDemo")
    public AjaxResult addSave(SysDemo sysDemo) {
        return AjaxResult.success(sysDemoService.save(sysDemo));
    }

    /**
     * 修改保存用户演示
     */
    @Log(title = "用户演示", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ApiOperation("更新用户")
    @ApiImplicitParam(name = "sysDemo", value = "新增用户信息", dataType = "SysDemo")
    public AjaxResult editSave(SysDemo sysDemo) {
        return AjaxResult.success(sysDemoService.updateById(sysDemo));
    }

    /**
     * 删除用户演示
     */
    @Log(title = "用户演示", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ApiOperation("删除用户信息")
    @ApiImplicitParam(name = "ids", value = "用户ID", required = true, dataType = "String")
    public AjaxResult remove(String ids) {
        return AjaxResult.success(sysDemoService.removeByIds(Arrays.asList(Convert.toStrArray(ids))));
    }
}
