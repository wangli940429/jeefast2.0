package com.jeefast.project.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jeefast.project.system.domain.SysDemo;

/**
 * 用户演示 数据层
 *
 * @author jeefast
 * @date 2019-09-24
 */
public interface SysDemoMapper extends BaseMapper<SysDemo> {

}