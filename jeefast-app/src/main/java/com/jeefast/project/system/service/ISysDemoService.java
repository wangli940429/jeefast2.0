package com.jeefast.project.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jeefast.project.system.domain.SysDemo;

/**
 * 用户演示 服务层
 *
 * @author jeefast
 * @date 2019-09-24
 */
public interface ISysDemoService extends IService<SysDemo> {

}