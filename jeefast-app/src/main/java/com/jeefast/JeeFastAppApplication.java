package com.jeefast;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 启动程序
 * 
 * @author jeefast
 */
@EnableSwagger2
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class JeeFastAppApplication
{
    public static void main(String[] args) throws UnknownHostException
    {
        // System.setProperty("spring.devtools.restart.enabled", "false");
        //SpringApplication.run(JeeFastAppApplication.class, args);
        //System.out.println("(♥◠‿◠)ﾉﾞ  APP接口工程启动成功   ლ(´ڡ`ლ)ﾞ  \n" );
        
        ConfigurableApplicationContext application = SpringApplication.run(JeeFastAppApplication.class, args);
        Environment env = application.getEnvironment();
        String ip = InetAddress.getLocalHost().getHostAddress();
        String port = env.getProperty("server.port");
        String path = env.getProperty("server.servlet.context-path");
        System.out.println("\n----------------------------------------------------------\n\t" +
            "Application 接口工程启动成功 is running! Access URLs:\n\t" +
            "Local: \t\thttp://localhost:" + port + path + "\n\t" +
            "External: \thttp://" + ip + ":" + port + path + "\n\t" +
            "swagger-ui: \thttp://" + ip + ":" + port + path + "swagger-ui.html\n\t" +
            "Doc: \t\thttp://" + ip + ":" + port + path + "doc.html\n" +
            "----------------------------------------------------------");
    }
}
