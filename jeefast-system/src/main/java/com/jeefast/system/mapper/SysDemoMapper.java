package com.jeefast.system.mapper;

import com.jeefast.system.domain.SysDemo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 用户演示 数据层
 *
 * @author jeefast
 * @date 2019-09-24
 */
public interface SysDemoMapper extends BaseMapper<SysDemo> {

}