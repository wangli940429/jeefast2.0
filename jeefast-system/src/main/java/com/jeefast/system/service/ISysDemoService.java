package com.jeefast.system.service;

import com.jeefast.system.domain.SysDemo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 用户演示 服务层
 *
 * @author jeefast
 * @date 2019-09-24
 */
public interface ISysDemoService extends IService<SysDemo> {

}