package com.jeefast.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.jeefast.system.mapper.SysDemoMapper;
import com.jeefast.system.domain.SysDemo;
import com.jeefast.system.service.ISysDemoService;

/**
 * 用户演示 服务层实现
 *
 * @author jeefast
 * @date 2019-09-24
 */
@Service
public class SysDemoServiceImpl extends ServiceImpl<SysDemoMapper, SysDemo> implements ISysDemoService {

}