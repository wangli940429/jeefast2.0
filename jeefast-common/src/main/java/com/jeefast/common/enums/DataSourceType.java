package com.jeefast.common.enums;

/**
 * 数据源
 * 
 * @author jeefast
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
